all: clean runid2 runid6 runid10

.PHONY: clean
clean:
	echo 'drop table if exists collisions;' | sqlite3 HighTraffic_PLOEG_mpr_0_fer_0.db
	echo 'drop table if exists collisions;' | sqlite3 HighTraffic_PLOEG_mpr0.4_per0.db
	echo 'drop table if exists collisions;' | sqlite3 HighTraffic_PLOEG_mpr_0.7_fer_0.db

.PHONY: runid2
runid2:
	./ttc_stats.py HighTraffic_PLOEG_mpr_0_fer_0.db net.xml --ttc=10

.PHONY: runid6
runid6:
	./ttc_stats.py HighTraffic_PLOEG_mpr0.4_per0.db net.xml --ttc=10

.PHONY: runid10
runid10:
	./ttc_stats.py HighTraffic_PLOEG_mpr_0.7_fer_0.db net.xml --ttc=10

